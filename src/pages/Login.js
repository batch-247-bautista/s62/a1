import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login(){

  const { user, setUser } = useContext(UserContext);

  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isActive, setIsActive] = useState("true");

 function authenticate(e) {
  e.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: email,
      password: password
    })
  })
  .then(res => res.json())
  .then(data => {
    if(typeof data.access !== "undefined"){
      localStorage.setItem('token', data.access);
      retrieveUserDetails(data.access);
      
      if (user.isAdmin) {
        Swal.fire({
          title: 'Login successful',
          icon: 'success',
          html: '<p>Welcome Admin!</p><p>You have successfully logged in to Shoes Mio.</p>'
        });
      } else {
        Swal.fire({
          title:'Login successful',
          icon: 'success',
          text: 'Welcome to Shoes Mio'
        });
      }
    } else {
      Swal.fire({
        title: "Authentication Failed!",
        icon: 'error',
        text: 'Please, check your login details and try again!'
      });
    }
  }); 

  setEmail("");
  setPassword("");
};


  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });
    });
  };

  useEffect(() => {
    setIsActive(email !== "" && password !== "");
  }, [email, password]);

  useEffect(() => {
    // Redirect user to appropriate page after login
    if(user.id !== null){
      navigate(user.isAdmin ? '/products/all' : '/products');
    }
  }, [user, navigate]);

  return (
    <div className="login-box">
      <Form onSubmit={(e) => authenticate(e)}>
        <Form.Group controlId="userEmail">
          <Form.Label>Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email here"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>
        { isActive ? 
          <Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>
          : 
          <Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>
        }
      </Form>
      <div className="primary my-3">
        <a href="/register" className="text-dark text-decoration-none">Register</a>
      </div>
      <div className="text-center">
        <a href="/register" className="text-muted text-decoration-none">Forgot your password?</a>
      </div>
    </div>
  )
}

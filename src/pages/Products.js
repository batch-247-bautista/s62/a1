
import ProductCard from '../components/ProductCard';
import Loading from '../components/Loading';

import { useState, useEffect } from 'react';

export default function Products() {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isAdmin, setIsAdmin] = useState(false); // assuming isAdmin state is declared here

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setProducts(data);
        setIsLoading(false);
      })
  }, []);

  const renderProductCard = (product) => {
    if (isAdmin) {
      // render sizes instead of product details
      return (
        <div key={product._id}>
          <h3>{product.name}</h3>
          <p>Sizes:</p>
          <ul>
            {product.sizes.map(size => (
              <li key={size}>{size}</li>
            ))}
          </ul>
        </div>
      )
    } else {
      // render product details
      return <ProductCard key={product._id} product={product} />;
    }
  }

  return (
    isLoading ? (
      <Loading />
    ) : (
      <>
        {products.map(product => renderProductCard(product))}
      </>
    )
  )
}


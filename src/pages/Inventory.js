import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Table, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

function Inventory() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`https://capstone-00002.onrender.com/products/all`)
      .then(res => res.json())
      .then(data => setProducts(data))
      .catch(err => console.error(err));
  }, []);

  const updateProduct = (productId, isActive, isArchived) => {
    console.log('updating product', productId, isActive, isArchived);

    const url = isActive
      ? `http://localhost:4005/products/${productId}/activate`
      : `https://capstone-00002.onrender.com/products/${productId}/archive`;

    console.log('fetching', url);

    fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({ isActive, isArchived }),
    })
      .then(res => {
        console.log('response object:', res);
        return res.json();
      })
      .then(data => {
        console.log('update success', data);
        setProducts(prevProducts => {
          const index = prevProducts.findIndex(p => p._id === productId);
          if (index === -1) return prevProducts;
          const updatedProduct = { ...prevProducts[index], isActive, isArchived };
          return [
            ...prevProducts.slice(0, index),
            updatedProduct,
            ...prevProducts.slice(index + 1),
          ];
        });

        if (isActive) {
          console.log('isActive is true');
          Swal.fire({
            icon: 'success',
            title: 'Availability Successful!',
            text: 'Product is now available',
            confirmButtonText: 'OK',
          });
        } else {
          console.log('isActive is false');
          Swal.fire({
            icon: 'success',
            title: 'Archive Successful!',
            text: 'Product is now unavailable',
            confirmButtonText: 'OK',
          });
        }
      })
      .catch(err => {
        console.error(err);
        console.log('update failed');
      });
  };

  const toggleProduct = (productId, isActive) => {
    const index = products.findIndex(p => p._id === productId);
    if (index === -1) return;
    const product = products[index];
    const isArchived = product.isArchived;
    const updatedProduct = { ...product, isActive: !isActive };
    setProducts(prevProducts => {
      const updatedProducts = [...prevProducts];
      updatedProducts[index] = updatedProduct;
      return updatedProducts;
    });
    console.log('toggleProduct:', productId, isActive, isArchived);
    updateProduct(productId, !isActive, isArchived);
  };
  console.log(products);

  


  return (
  <div>
    <h2>Inventory</h2>
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Product ID</th>
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Active</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {products.map(product => (
          <tr key={product._id}>
            <td>{product._id}</td>
            <td>{product.name}</td>
            <td>{product.description}</td>
            <td>{product.price}</td>
            <td>{product.isActive ? 'Active' : 'Inactive'}</td>
            <td>
              <div className="d-flex flex-column">
                <Button
                  variant="primary"
                  as={Link}
                  to={`/products/${product._id}`}
                  className="mb-2"
                >
                  View
                </Button>
                {product.isActive ? (
                  <Button
                    variant="warning"
                    onClick={() => toggleProduct(product._id, true)}
                    className="mb-2"
                  >
                    Deactivate
                  </Button>
                ) : (
                  <Button
                    variant="success"
                    onClick={() => toggleProduct(product._id, false)}
                    className="mb-2"
                  >
                    Activate
                  </Button>
                )}
                <Link to={`/update-product/${product._id}`}>
                <Button variant="info" className="mb-2">
                  Update
                </Button>
              </Link>
              </div>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  </div>
);
  }

export default Inventory;






